using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class escaladocapsula : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;
    private float scaleUnits;
    void Update()
    {
        axes = controlcapsula.ClampVector3(axes);
        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}

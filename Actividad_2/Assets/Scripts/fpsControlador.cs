using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(charactermovement))]
[RequireComponent(typeof(mauselook))]
public class fpsControlador : MonoBehaviour
{
    private charactermovement charactermovement;
    private mauselook mauselook;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("Capsule").gameObject.SetActive(false);

        charactermovement = GetComponent<charactermovement>();
        mauselook = GetComponent<mauselook>();
    }

    // Update is called once per frame
    void Update()
    {
        movement();
        rotation();
    }
    private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        charactermovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mauselook.handleRotation(hRotationInput, vRotationInput);
    }
}


